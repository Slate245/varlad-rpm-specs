%global debug_package %{nil}

Name:       elm
Version:    0.19.1
Release:    1%{?dist}
Summary:    Compiler for Elm, a functional language for reliable webapps.  

License:    BSD-3-Clause license 
URL:        https://github.com/elm/compiler
Source0:    %{url}/releases/download/%{version}/binary-for-linux-64-bit.gz

Recommends: glibc-langpack-en

%description
Elm is a functional language that compiles to JavaScript. It helps you make websites and web apps. It has a strong emphasis on simplicity and quality tooling.

%prep
%autosetup -n .

%install
mkdir -p %{buildroot}%{_bindir}
mv ./binary-for-linux-64-bit %{buildroot}%{_bindir}/elm

%files
%{_bindir}/elm
